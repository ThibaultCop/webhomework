<?php
    
    function price($nbpers, $time) {
        $price=0;
        if (($time <= "15:00:00")&&($time >= "12:00:00"))
        {
            return($nbpers * 2);
        }   
        elseif (($time <= "23:00:00") && ($time >= "19:30:00"))
        {
            return($nbpers * 3);
        }
    } 
   
    if (isSet($_POST['nbpers']) && isSet($_POST['name']) && isSet($_POST['date']) && isSet($_POST['time']) && isSet($_POST['name']) && isSet($_POST['tel'])) {


        $nbpers=$_POST['nbpers'];
        $name=$_POST['name'];
        $date=$_POST['date'];
        $time=$_POST['time'];
        $tel=$_POST['tel'];
        $problem="";
        $pb=false;
        $today=date("Y-m-d");

        if (($nbpers < 1)&&($nbpers > 10)) { $problem=$problem."Nb of person invalid : 1 min, 10 max."; $pb=true;}
        if ( $date < $today ) { $problem=$problem."Date invalid, must be at least tomorrow"; $pb=true;}
        if ( ($time> "23:00:00")||($time< "12:00:00")||(($time < "19:30:00")&&($time>"15:00:00")) ){ $problem=$problem."Restaurant not open at that time"; $pb=true;}

        if ($pb){
            include('lab9_pbView.php'); 
        }
        else
        {
            $price= price($_POST['nbpers'],$_POST['time']);
            include('lab9_resaView.php');
        }
    }
       
?>