function password()
{
	
	var passlength = document.getElementById("pass1").value.length;
	var passanswer=" ";
	
	if(document.getElementById("pass1").value==document.getElementById("pass2").value)
	{
		document.getElementById("passidentical").innerHTML="Identical passwords.<br>";
		if(passlength <8)
		{
			passanswer=passanswer.concat("Password too short, it must contain at least 8 characters. <br>");
		}
		if(document.getElementById("pass1").value.search(/[A-Z]/)==-1)
		{
			passanswer=passanswer.concat("Password must contain at least 1 capital letter.<br>");
		}
		if(document.getElementById("pass1").value.search(/[!-/]/)==-1)
		{
			passanswer=passanswer.concat("Password must contain at least one of the following characters: !, &quot, #, $,',(, ), *, +, -, ., / <br>");
		}
		if(document.getElementById("pass1").value.search(/[0-9]/)==-1)
		{
			passanswer=passanswer.concat("Password must contain at least one number<br>");
		}
		if(document.getElementById("pass1").value.search(/[a-z]/)==-1)
		{
			passanswer=passanswer.concat("Password must contain at least one lowercase letter<br>");
		}
		else if ( (document.getElementById("pass1").value.search(/[a-z]/)!=-1) && (document.getElementById("pass1").value.search(/[0-9]/)!=-1) && (document.getElementById("pass1").value.search(/[!-/]/)!=-1) && (document.getElementById("pass1").value.search(/[A-Z]/)!=-1) && (passlength <8) )
		{
			document.getElementById("passidentical").innerHTML="Password successfully created";
		}
	}
	else if (document.getElementById("pass1").value!=document.getElementById("pass2").value)
	{
		passanswer=passanswer.concat("Confirmation doesn't correspond with password <br>");
	}
	
	
	document.getElementById("passanswer").innerHTML = passanswer;
}



function cart()
{
	var shirtcnt = document.getElementById("shirt").value;
	var capcnt = document.getElementById("cap").value;
	var hoodiecnt = document.getElementById("hoodie").value;
	var VAT= 15/100;
	
	var price = (shirtcnt*15 + capcnt*25 + hoodiecnt*35) ;
	var priceVAT = price * VAT;

	if( (shirtcnt<0) || (capcnt<0) || (hoodiecnt<0) )
	{
		document.getElementById("cartprice").innerHTML = "ERROR : You have entered a negative product amount";
	}
	else
	{
		document.getElementById("cartprice").innerHTML = "Cart Price : " + price + "$ <br>           including "+priceVAT+"$ VAT";
	}
	
	
}



function Verify()
{
	
	var infoverif =" ";
	var d = new Date(1998,08,30);
	var prob=false;
	
	
	
	if((document.getElementById("hfirstname").value.search(/[0-9]/)!=-1) || (document.getElementById("hfirstname").value.search(/[!-/]/)!=-1))
		{
			
			infoverif=infoverif.concat("Names must contain only letters. <br>");
			prob=true;
			
		}
	if((document.getElementById("hlastname").value.search(/[0-9]/)!=-1) || (document.getElementById("hlastname").value.search(/[!-/]/)!=-1))
		{
			
			infoverif=infoverif.concat("Names must contain only letters. <br>");
			prob=true;
			
		}
	if(document.getElementById("hzipcode").value.search(/[!-/]/)==-1)
		{
			infoverif=infoverif.concat("Zipcode can't contain any letters. <br>");
			prob=true;
			
		}
	if(document.getElementById("hmail").value.search(/@/)==-1)
		{
			infoverif=infoverif.concat("Email not valid. <br>");
			prob=true;
			
		}
	if(document.getElementById("hbirthdate").value > d )
		{
			infoverif=infoverif.concat("You need to be an adult to rent a room. <br>");
			prob=true;
			
		}
	if(document.getElementById("hadate").value > document.getElementById("hddate").value  )
		{
			infoverif=infoverif.concat("Departure date must be after your arrival date <br>");
			prob=true;
			
		}
	if(document.getElementById("hcardnb").value.length != 16 )
		{
			infoverif=infoverif.concat("Card Number must have 16 numbers. <br>");
			prob=true;
			
		}
	if(document.getElementById("hsecunb").value.length != 3 )
		{
			infoverif=infoverif.concat("Secu number must have 3 numbers and is located on the back of your card. <br>");
			prob=true;
			
		}
		
	if(prob)
	{
		document.getElementById("badverifansw").innerHTML = infoverif;
	}
	else
	{
		document.getElementById("goodverifansw").innerHTML = "Room successfully registered"
	}
}




