<!DOCTYPE html>
<html>
	<head>
		<title>PHPLab</title>
		<link rel="stylesheet" href="style.css"/>
	</head>
	
	
	<body>
		
		<h1>LAB 8 : INTRODUCTION TO PHP</h1>
		
		<?php
		
		
			function table($array)
			{
				print "<h3>The table is : </h3><br><table class=table><tr>";
				for($i=0;$i<sizeof($array);$i++)
				{
					print "<td class=table>".$array[$i]."</td>";
				}
				print"</tr></table>";
			}
			
			function average($array)
			{
				$i = 0;
				$sum = 0;
				$average = 0;
				for($x = 0;$x < count($array); $x++)
				{
					$sum = $array[$x] + $sum;
					$i++;
				}
				$average = $sum / $i ;
				print "<h3> The average is : ".round($average,2)."</h3>";
			}
			
			
			function median($array)
			{
				
				$median = 0;
				
				sort($array);
				
				if (  sizeof($array)%2 != 0 )
				{
					$median =  $array[sizeof($array)/2 -0.5];
				}
				else{
				
					$median = ( $array[sizeof($array)/2] + $array[sizeof($array)/2 -1] ) /2 ;
				} 
				print "<h3> The median is : ".$median."</h3>";
			} 
			
			
			
			function order($array)
			{
				sort($array);
				print "<h3>The ordered table is :</h3>";
				print"<ol>";
				for($x = 0;$x < count($array); $x++)
				{
					print"<li>".$array[$x]."</li>";
				}
				print"</ol>";
			}


			function reverseorder($array)
			{
				rsort($array);
				print "<h3>The reverse-ordered table is :</h3>";
				print"<ol>";
				for($x = 0;$x < count($array); $x++)
				{
					print"<li>".$array[$x]."</li>";
				}
				print"</ol>";
			}
			
			function squarecube($array)
			{
				print "<h3>Squares and Cubes:</h3><br><table class=table><tr><td>Squares :</td>";
				for($i=0;$i<sizeof($array);$i++)
				{
					print"<td class=table>".pow($array[$i], 2)."</td>";
				}
				print "</tr><tr><td>Cubes :</td>";
				
				for($i=0;$i<sizeof($array);$i++)
				{
					print"<td class=table>".pow($array[$i], 3)."</td>";
				}
				print "</tr></table>";
			}
			
			function tax($price, $tax)
			{
				print "<h3>How to calculate the final price of a taxed product :</h3>";
				print"<br>Price : ".$price;
				print"<br>Tax : ".$tax."%";
				$newprice=round($price + ($tax*$price/100),2);
				print "<br>Price with taxes : ".$newprice;
			}
			
			$array = array(10,25,20,15,5);
			$price=150;
			$tax=10;
			
			table($array);
			average($array); 
			median($array);
			order($array);
			reverseorder($array);
			squarecube($array);
			tax($price,$tax);
		?>
		
	
		<h3> Questions</h3>
		<ol>
		<li>the phpinfo functions, display a lot of informations about the php configuration as extensions, options f compilation etc...</br><i>info general : </i>gave us the configuration line, the php.ini acess, the date of compilation, web servor etc....<br><i>Info_variables : </i>display all the variable predefined from the get method, post method, cookies etc...<br><i>Info_all : </i>display all the informations of our php configuration</li>
		<li>you have to increase the size and the power of your server to allow it to manage big database, and big fluctuation on your server.</li>
		<li>it means that the html code, creating the webpage the user will see, is running on a server : in brief, it means that the client access to a file wich is not  in his computer, by an url : the website is online.</li>
		</ol>
	
	
	
	</body>
	
	
</html>