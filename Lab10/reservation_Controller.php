<?php
    
    function price($nbpers, $restime, $lunchprice, $dinnerprice) {
       
        if (($restime <= "15:00:00")&&($restime >= "12:00:00"))
        {
            return($nbpers * $lunchprice);
        }   
        elseif (($restime <= "23:00:00") && ($restime >= "19:30:00"))
        {
            return($nbpers * $dinnerprice);
        }
    } 
   




    if (isSet($_POST['nbpers']) && isSet($_POST['name']) && isSet($_POST['date']) && isSet($_POST['time']) && isSet($_POST['name']) && isSet($_POST['tel'])) {

        include('reservation_Model.php');

        $lunchprice=getprice("lunch",$conn);        
        $dinnerprice=getprice("dinner",$conn);

        echo $lunchprice;
        echo $dinnerprice;


        $nbpers=$_POST['nbpers'];
        $name=$_POST['name'];
        $resdate=$_POST['date'];
        $restime=$_POST['time'];
        $tel=$_POST['tel'];
        $problem="";
        $pb=false;
        $today=date("Y-m-d");

        if (($nbpers < 1)&&($nbpers > 10)) { $problem=$problem."Nb of person invalid : 1 min, 10 max."; $pb=true;}
        if ( $resdate < $today ) { $problem=$problem."Date invalid, must be at least tomorrow"; $pb=true;}
        if ( ($restime> "23:00:00")||($restime< "12:00:00")||(($restime < "19:30:00")&&($restime>"15:00:00")) ){ $problem=$problem."Restaurant not open at that time"; $pb=true;}

        if ($pb){
            include('reservation_pbView.php'); 
        }
        else
        {
            $price= price($_POST['nbpers'],$_POST['time'],$lunchprice, $dinnerprice);
            echo $price;
            reservation($resdate, $restime, $tel, $name, $nbpers, $price, $conn);
            include('reservation_resultView.php');
        }
    }
       
?>