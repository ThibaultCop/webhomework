<?xml version="1.0" encoding="UTF-8"?>
<xsl:template match="/">

<html>
	
	<body style="background_color:black;">
		<h1 style="color:yellow;"><xsl:value-of select="Eminem_Discography/title"/></h1>
		<xsl:for-each select="Eminem_Discography/album">
			<table style="border: 2px white;">
			<tr style="border: 2px white;background_color:lightblue;">
				<th>Name</th>
				<th>Date</th>
				<th>Sales</th>
			</tr>
			<tr style="border: 2px white;">
				<td><xsl:value-of select="name"/></td>
				<td><xsl:value-of select="date"/></td>
				<td><xsl:value-of select="sales"/></td>
			</tr>
			</table>
			<br>
		</xsl:for-each>
	</body>
	
</html>